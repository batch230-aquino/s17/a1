/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFullName = function printName() {
  let firstName = prompt("What is Your Name? ");
  //   let age = prompt("How old are you?");
  //   let address = prompt("Where do you live? ");

  console.log("Hello, " + firstName);
};

printFullName();

let printUserAge = function printAge() {
  let age = prompt("How old are you?");
  console.log("You are " + age + " years old.");
};

printUserAge();

let printUserAddress = function printAddress() {
  let location = prompt("Where do you live?");
  console.log("You live in " + location);
  alert("Thank you for the Input");
};

printUserAddress();

function printMusicBand() {
  console.log("1. The Beatles");
  console.log("2. Metallica");
  console.log("3. The Eagles");
  console.log("4. L'arc~en~Ciel");
  console.log("5. Eraserheads");
}

printMusicBand();

function printMovie() {
  console.log("1. The Godfather");
  console.log("Rotten Tomatoes Rating: 97%");
  console.log("2. The Godfather, Part II");
  console.log("Rotten Tomatoes Rating: 96%");
  console.log("3. Shawshank Redemption");
  console.log("Rotten Tomatoes Rating: 91%");
  console.log("4. To Kill A Mocking Bird");
  console.log("Rotten Tomatoes Rating: 93%");
  console.log("5. Physco");
  console.log("Rotten Tomatoes Rating: 96%");
}

printMovie();

let printFriends = function printUsers() {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with:");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
};

printFriends();
